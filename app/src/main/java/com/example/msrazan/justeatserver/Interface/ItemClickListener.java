package com.example.msrazan.justeatserver.Interface;

import android.view.View;

/**
 * Created by MS RAZAN on 4/9/2018.
 */

public interface ItemClickListener {
    void onClick(View view, int position, boolean isLongClick);
}
