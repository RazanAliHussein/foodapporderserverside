package com.example.msrazan.justeatserver.Common;

import com.example.msrazan.justeatserver.Model.User;

/**
 * Created by MS RAZAN on 3/30/2018.
 */

public class Common {
    public static User currentUser;

    public static final String UPDATE ="Update";
    public static final String DELETE ="Delete";
    public static final int PICK_IMAGE_REQUEST=71;
    public static String convertCodeToStatus(String code){
        if(code.equals("0"))
            return "Placed";
        else if(code.equals("1"))
            return "On My Way";
        else
            return "Shipped";

    }
}
